﻿
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Debug = System.Diagnostics.Debug;

namespace BingPaper.Util {
	internal static class CountryListProvider {
		public static IEnumerable<BingCountry> BingCountries {
			get {
				var assembly = typeof(CountryListProvider).GetTypeInfo().Assembly;
				using (TextReader reader = new StreamReader(assembly.GetManifestResourceStream("DownloadBingPaper.Util.country_list.txt"))) {
					string line;
					while (null != (line = reader.ReadLine())) {
						string[] pair = line.Split(':');
						Debug.Assert(pair != null && pair.Length == 2);
						yield return new BingCountry {
							Name = string.Format("({0}) {1}", pair[0].Trim(), pair[1].Trim()),
							Code = pair[0].Trim()
						};
					}
				}
			}
		}
	}

	internal class BingCountry {
		public string Name { get; set; }
		public string Code { get; set; }
	}
}
