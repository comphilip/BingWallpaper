﻿namespace BingPaper.Interfaces {
    interface IConfig {
        string CountryCode { get; }
        bool RandomCountryCode { get; }
        string ImageSavePath { get; }
        string HttpProxy { get; }
    }
}