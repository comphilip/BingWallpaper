﻿namespace SetWindowsWallpaperInterfaces {
	interface IConfig {
		string WallPaperStyle { get; }
		string ImageSavePath { get; }
	}
}
