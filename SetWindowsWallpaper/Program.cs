using System;
using CommandLine;
using SetWindowsWallpaper.Impl.Command;
using SetWindowsWallpaper.Interfaces;


namespace SetWindowsWallpaper {
    internal sealed class Program {
        private static int Main (string[] args) {
            ILogger logger = new ConsoleLogger ();
            var config = new CommandOptions ();
            if (Parser.Default.ParseArgumentsStrict (args, config)) {
                try {
                    var context = new CommandContext {
                        Logger = logger,
                        Config = config
                    };
                    foreach (var cmd in new ICommand[] { new SetDesktopWallpaperCommand (), new SetWallpaperStyleCommand () }) {
                        cmd.Run (context);
                    }
                    return 0;
                } catch (Exception err) {
                    logger.Error (err.Message);
                    return 1;
                }
            } else {
                logger.Error ("parse command argument failed");
                return 1;
            }
        }
    }
}