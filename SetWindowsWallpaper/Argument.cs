﻿using System;
using System.Linq;

namespace SetWindowsWallpaper {
	public static class Argument {
		public static void NotNull(object value, string paramName = null) {
			if (value == null)
				throw new ArgumentNullException(paramName);
		}

		public static void OutOfRange(bool condition, string paramName = null) {
			if (condition)
				throw new ArgumentOutOfRangeException(paramName);
		}

		public static void InvalidArgument(bool condition, string message) {
			if (condition)
				throw new ArgumentException(message);
		}

		public static void NotNullOrEmpty(string value, string paramName = null) {
			if (string.IsNullOrEmpty(value))
				throw new ArgumentNullException(paramName);
		}
	}
}
