using System.Runtime.InteropServices;
namespace SetWindowsWallpaper.PInvoke {
	internal struct ANIMATIONINFO {
		public uint cbSize;
		public int iMinAnimate;
		public ANIMATIONINFO(int iMinAnimate) {
			this.cbSize = (uint)Marshal.SizeOf(typeof(ANIMATIONINFO));
			this.iMinAnimate = iMinAnimate;
		}
	}
}
