﻿using System;
using BingPaper.Impl;
using BingPaper.Impl.Command;
using BingPaper.Interfaces;
using CommandLine;

namespace BingPaper {
    class Program {
        static int Main (string[] args) {
            ILogger logger = new ConsoleLogger ();
            var config = new CommandOptions();
            if (Parser.Default.ParseArgumentsStrict(args, config)) {
                try {
                    var context = new CommandContext {
                        Logger = logger,
                        Config = config
                    };
                    var cmd = new DownloadImageCommand ();
                    cmd.Run (context);
                    return 0;
                } catch (Exception err) {
                    logger.Error (err.Message);
                    return 1;
                }
            } else {
                logger.Error ("parse command argument failed");
                return 1;
            }
        }
    }
}