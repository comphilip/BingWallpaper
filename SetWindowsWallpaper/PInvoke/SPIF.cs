using System;
namespace SetWindowsWallpaper.PInvoke {
	[Flags]
	internal enum SPIF {
		None = 0,
		SPIF_UPDATEINIFILE = 1,
		SPIF_SENDCHANGE = 2,
		SPIF_SENDWININICHANGE = 2
	}
}
