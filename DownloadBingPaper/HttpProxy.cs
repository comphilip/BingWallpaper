using System;
using System.Net;
using BingPaper;

namespace BingPaper {
    class HttpProxy : IWebProxy {
        private string _httpProxy;

        public HttpProxy (string httpProxy) {
            Argument.NotNullOrEmpty (httpProxy, nameof (httpProxy));
            _httpProxy = httpProxy;
        }

        public ICredentials Credentials { get; set; }

        public Uri GetProxy (Uri destination) {
            return new Uri (_httpProxy);
        }

        public bool IsBypassed (Uri host) {
            return false;
        }
    }
}