using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using BingPaper.Interfaces;
using Newtonsoft.Json.Linq;

namespace BingPaper.Impl {
    internal class ImageDownloader {
        private const string LIST_URL = "/HPImageArchive.aspx?format=js&idx=0&n=1&setmkt={0}";

        private const string HD_IMAGE_URL = "/hpwp/{0}?FORM=HDRHME&setmkt={1}";

        private static readonly Uri BASE_URL = new Uri ("http://global.bing.com");

        private ILogger _logger;
        private string _savedPath;
        private string _httpProxy;

        public ImageDownloader (ILogger logger, string savedPath, string httpProxy) {
            Argument.NotNull (logger, nameof (logger));
            Argument.NotNullOrEmpty (savedPath, nameof (savedPath));
            _logger = logger;
            _savedPath = savedPath;
            _httpProxy = httpProxy;
        }

        public string DownloadImage (string countryCode) {
            var imageInfo = this.GetImageName (countryCode);
            this._logger.Info ("Bing image name is " + imageInfo.Key);
            this.DownloadImage (imageInfo.Key, imageInfo.Value, countryCode);
            return imageInfo.Key;
        }

        private KeyValuePair<String, String> GetImageName (string countryCode) {
            this._logger.Info ("Get Bing image name...");
            Uri requestUri = new Uri (ImageDownloader.BASE_URL, string.Format (LIST_URL, countryCode));
            var webRequest = CreateWebRequest (requestUri);
            WebResponse response = webRequest.GetResponseAsync ().Result;
            var result = new KeyValuePair<string, string> (string.Empty, string.Empty);
            using (TextReader textReader = new StreamReader (response.GetResponseStream ())) {
                var jsonData = JObject.Parse (textReader.ReadToEnd ());
                var jsonData2 = jsonData["images"];
                if (jsonData2.Count () > 0) {
                    result = new KeyValuePair<string, string> (jsonData2[0]["url"].ToString (), jsonData2[0]["hsh"].ToString ());
                }
            }
            return result;
        }

        private void DownloadImage (string lowQulityPath, string imageHsh, string countryCode) {
            try {
                DownloadHDImage (imageHsh, countryCode);
            } catch (Exception err) {
                _logger.Error (string.Format ("Download HD Image failed : {0}", err.Message));
                _logger.Info ("Download Low Image");
                DownloadLDImage (lowQulityPath);
            }
        }

        private void DownloadHDImage (string imageHsh, string countryCode) {
            Uri imageUri = new Uri (BASE_URL, string.Format (HD_IMAGE_URL, imageHsh, countryCode));
            Download (imageUri);
        }

        private void DownloadLDImage (string lowQulityPath) {
            Uri imageUri = new Uri (BASE_URL, lowQulityPath);
            Download (imageUri);
        }

        private void Download (Uri imageUri) {
            this._logger.Info ("Download Image...");
            var webRequest = CreateWebRequest (imageUri);
            WebResponse response = webRequest.GetResponseAsync ().Result;
            using (Stream responseStream = response.GetResponseStream ()) {
                using (Stream fileStream = new FileStream (_savedPath, FileMode.Create, FileAccess.Write, FileShare.None)) {
                    responseStream.CopyTo (fileStream);
                }
            }
            this._logger.Info ("Image saved at " + _savedPath);
        }
        private WebRequest CreateWebRequest (Uri uri) {
            WebRequest webRequest = WebRequest.Create (uri);
            if (string.IsNullOrWhiteSpace (_httpProxy)) {
                _logger.Info("Using system default proxy");
                webRequest.Proxy = WebRequest.DefaultWebProxy;
            } else {
                _logger.Info($"Using HTTP Proxy: {_httpProxy}");
                webRequest.Proxy = new HttpProxy (_httpProxy);
            }
            webRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            return webRequest;
        }
    }
}