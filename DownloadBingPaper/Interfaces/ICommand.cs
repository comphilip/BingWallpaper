﻿
namespace BingPaper.Interfaces {
	class CommandContext {
		public ILogger Logger { get; set; }
		public IConfig Config { get; set; }
	}

	interface ICommand {
		void Run(CommandContext context);
	}
}
