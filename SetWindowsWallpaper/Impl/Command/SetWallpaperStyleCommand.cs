﻿using Microsoft.Win32;
using System;
using SetWindowsWallpaper.Interfaces;
using SetWindowsWallpaper.Util;

namespace SetWindowsWallpaper.Impl.Command {
	class SetWallpaperStyleCommand : ICommand {
		public void Run(CommandContext context) {
			if (context == null)
				throw new ArgumentNullException("context");
			context.Logger.Info(string.Format("Set wallpaper style: {0}", context.Config.WallPaperStyle));
			var style = WallPaperStyleListProvider.StyleSettingValues[WallPaperStyleListProvider.StyleNameValues[context.Config.WallPaperStyle]];
			SetWallpaperStyle(style.WallpaperStyle, style.TileWallPaper, context.Logger);
		}

		private void SetWallpaperStyle(int wallpaperStyle, int tileWallpaper, ILogger logger) {
			var key = Registry.CurrentUser;
			foreach (var name in new string[] { "Control Panel", "Desktop" }) {
				key = key.OpenSubKey(name, true);
				if (key == null) {
					logger.Error(string.Format("not found registry key : {0}", name));
					return;
				}
			}
			key.SetValue("WallpaperStyle", wallpaperStyle.ToString());
			key.SetValue("TileWallpaper", tileWallpaper.ToString());
		}
	}
}
