using System;
using BingPaper.Interfaces;
using CommandLine;
using CommandLine.Text;

namespace BingPaper {
    class CommandOptions : IConfig {

        [Option ("country-code", DefaultValue = "en-US", HelpText = "country code of bing image, default is en-US")]
        public string CountryCode { get; set; }

        [Option ("random-country-code", DefaultValue = false, HelpText = "random select a country code")]
        public bool RandomCountryCode { get; set; }

        [Option ("image-save-path", Required = true, HelpText = "bing image save path")]
        public string ImageSavePath { get; set; }

        [Option ("http-proxy", HelpText = "http proxy")]
        public string HttpProxy { get; set; }
    }
}