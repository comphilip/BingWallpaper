using System;
using CommandLine;
using SetWindowsWallpaperInterfaces;

namespace SetWindowsWallpaper {
    class CommandOptions : IConfig {
        [Option ("style", DefaultValue = "Stretch", HelpText = "Wallpaper fill style: Center, Stretch, Fit, Fill, Tile. Default is Stretch")]
        public string WallPaperStyle { get; set; }
        [Option ("image-path", Required = true, HelpText = "Image path")]
        public string ImageSavePath { get; set; }
    }
}