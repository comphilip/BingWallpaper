﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace SetWindowsWallpaper.Util {
	internal class WallPaperStyleSetting {
		public int WallpaperStyle { get; set; }
		public int TileWallPaper { get; set; }
	}

	internal static class WallPaperStyleListProvider {
		private static string[] _names = { "Center", "Stretch", "Fit", "Fill", "Tile" };
		private static int[][] _values = {
			new int[]{0, 0},
			new int[]{2, 0},
			new int[]{6, 0},
			new int[]{10, 0},
			new int[]{0, 1}
		};

		public static IDictionary<string, int> StyleNameValues {
			get {
				return Enumerable.Range(0, _names.Length)
					.ToDictionary(x => _names[x], x => x, StringComparer.OrdinalIgnoreCase);
			}
		}

		public static IDictionary<int, WallPaperStyleSetting> StyleSettingValues {
			get {
				return Enumerable.Range(0, _values.Length)
					.ToDictionary(x => x, x => new WallPaperStyleSetting { WallpaperStyle = _values[x][0], TileWallPaper = _values[x][1] });
			}
		}
	}
}
