﻿using SetWindowsWallpaper.Interfaces;
using SetWindowsWallpaper.PInvoke;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace SetWindowsWallpaper.Impl.Command {
	class SetDesktopWallpaperCommand : ICommand {
		public void Run(CommandContext context) {
			if (context == null)
				throw new ArgumentNullException("context");
			var fullImagePath = Path.GetFullPath(context.Config.ImageSavePath);
			context.Logger.Info(string.Format("Set wallpaper: {0}", fullImagePath));
			if (!SPIWrapper.SystemParametersInfo(SPI.SPI_SETDESKWALLPAPER, 0u, fullImagePath,
				SPIF.SPIF_UPDATEINIFILE | SPIF.SPIF_SENDWININICHANGE | SPIF.SPIF_SENDCHANGE)) {
				int lastWin32Error = Marshal.GetLastWin32Error();
				context.Logger.Error(String.Format("set wallpaper failed : 0x{0:X8}", lastWin32Error));
			}
		}
	}
}
