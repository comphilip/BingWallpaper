﻿using System;
using System.Linq;
using BingPaper.Interfaces;
using BingPaper.Util;


namespace BingPaper.Impl.Command {
    class DownloadImageCommand : ICommand {
        public void Run (CommandContext context) {
            if (context == null)
                throw new ArgumentNullException ("context");
            ImageDownloader downloader = new ImageDownloader (context.Logger,
                context.Config.ImageSavePath,
                context.Config.HttpProxy
            );
            var imageName = downloader.DownloadImage (GetCountryCode (context.Config));
        }

        private string GetCountryCode (IConfig config) {
            if (!config.RandomCountryCode)
                return config.CountryCode;

            var list = CountryListProvider.BingCountries;
            Random random = new Random ();
            return list.ElementAt (random.Next (0, list.Count ())).Code;
        }
    }
}