using System;
using BingPaper.Interfaces;

namespace BingPaper.Impl {
	internal class ConsoleLogger : ILogger {
		public void Info(string msg) {
			Console.WriteLine("[INFO] {0}", msg);
		}
		public void Warn(string msg) {
			Console.WriteLine("[Warn] {0}", msg);
		}
		public void Error(string msg) {
			Console.WriteLine("[Error] {0}", msg);
		}
	}
}
