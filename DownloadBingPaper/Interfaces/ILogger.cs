namespace BingPaper.Interfaces {
	internal interface ILogger {
		void Info(string msg);
		void Warn(string msg);
		void Error(string msg);
	}
}
